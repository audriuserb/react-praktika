import React from 'react';
import UserInfo from './userinfo';
import formatDate from './formatdate';
function Comment(props) {
    return (
      <div className="Comment">
        <UserInfo user={props.author} />
        <div className="Comment-text">{props.text}</div>
        <div className="Comment-date">
          {formatDate(props.date)}
        </div>
        <div className="About">{props.about}</div>
      </div>
    );
  }
export default Comment;