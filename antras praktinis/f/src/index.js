import React from 'react';
import ReactDOM from 'react-dom';
import Comment from './comment';
  
  const comment = {
    date: new Date(),
    text: 'I hope you enjoy learning React!',
    author: {
      name: 'Audrius Serbentavičius',
      avatarUrl: 'https://placekitten.com/g/64/64',
    },
    about: 'Mėgstu kebabus',
  };

ReactDOM.render(
    <Comment
        date={comment.date}
        text={comment.text}
        about={comment.about}
        author={comment.author}/>,
    document.getElementById('app'),
)