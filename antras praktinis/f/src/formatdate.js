import React from 'react';
function formatDate(date) {
    return date.toLocaleDateString();
  }
export default formatDate;