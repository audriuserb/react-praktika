import React from 'react';
import UserInfo from './userinfo';
import formatDate from './formatdate';
class Comment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {friendsListHandle: false, moreInfo: false};
    this.handleFriendsClick = this.handleFriendsClick.bind(this);
    this.moreInfoClick = this.moreInfoClick.bind(this);
  }

  handleFriendsClick() {
    this.setState(prevState => ({
      friendsListHandle: !prevState.friendsListHandle
    }));
  }

  moreInfoClick() {
    this.setState(prevState => ({
      moreInfo: !prevState.moreInfo
    }));
  }

  render(){
  const friendsList = ['Tadas','Tomas','Ryčka','Mantas','Justas'];
  const moreInfo = this.state.moreInfo;
  const friendsListHandle = this.state.friendsListHandle;
  return (
      <div className="Comment">
        <div onClick={this.moreInfoClick} >
        <UserInfo user={this.props.author} />
        </div>
        <h3 onClick={this.handleFriendsClick}>
          Draugų sąrašas
        </h3>
        {friendsListHandle
          ? <ol>
              {friendsList.map(x => <li>{x}</li>)}
            </ol>
          : null
        }
        {moreInfo
        ? <div onClick={this.handleFriendsClick}>
          <div className="Comment-text">{this.props.text}</div>
            <div className="Comment-date">
              {formatDate(this.props.date)}
            </div>
          <div className="About">{this.props.about}</div>
          </div>
        : null
        }
      </div>
    );
    }
  }
export default Comment;