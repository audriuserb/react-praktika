import React from 'react';
import Name from './name';
class NameForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {value: '', isSubmitted: false};
  
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange(event) {
      this.setState({value: event.target.value});
    }
  
    handleSubmit(event) {
      this.setState({isSubmitted: true});
      event.preventDefault();
    }
  
    render() {
    const isSubmitted = this.state.isSubmitted;
      return (
        <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Name:
            <input type="text" value={this.state.value} onChange={this.handleChange} />
          </label>
          <input type="submit" value="Submit" />
        </form>
        {isSubmitted
        ?<Name inputname ={this.state.value} />
        :null
        }
        </div>
      );
    }
  }
export default NameForm;