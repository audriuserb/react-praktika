import React from 'react';
function Essay(props){
    const value = props.value;

    return(
        <div>
            Submitted essay: {value}
        </div>
    );
}
export default Essay;