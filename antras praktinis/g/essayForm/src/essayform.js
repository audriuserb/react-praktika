import React from 'react';
import Essay from './essay';
class EssayForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        value: 'Please write an essay about your favorite DOM element.',
        isSubmitted: false
      };
  
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange(event) {
      this.setState({value: event.target.value});
    }
  
    handleSubmit(event) {
      this.setState({isSubmitted: true});
      alert('An essay was submitted: ' + this.state.value);
      event.preventDefault();
    }
  
    render() {
        const isSubmitted = this.state.isSubmitted;
      return (
        <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Essay:
            <textarea value={this.state.value} onChange={this.handleChange} />
          </label>
          <input type="submit" value="Submit" />
        </form>
        {isSubmitted
            ? <Essay value ={this.state.value} />
            : null
        }
        </div>
      );
    }
  }
export default EssayForm;