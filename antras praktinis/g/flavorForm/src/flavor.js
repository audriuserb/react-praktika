import React from 'react';
function Flavor(props){
    const value = props.value;
    const name = props.name;
    return(
        <div>
            {name + " mėgsta " + value + " skonį."}
        </div>
    );
}
export default Flavor;