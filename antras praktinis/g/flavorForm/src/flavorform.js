import React from 'react';
import Flavor from './flavor';
class FlavorForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {value: 'apelsinas', vardas: ' '};
      this.selectInput = React.createRef();
      this.textInput = React.createRef();
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleSubmit(event) {
        event.preventDefault();
        this.setState({isSubmitted: true, value: this.selectInput.current.value, name: this.textInput.current.value});
    }
  
    render() {
    const isSubmitted = this.state.isSubmitted;
      return (
        <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Pick your favorite flavor:
            <select ref={this.selectInput}>
              <option value="apelsinų">apelsinas</option>
              <option value="laimo">laimas</option>
              <option value="kokoso">kokosas</option>
              <option value="mango">mangas</option>
            </select>
          </label>
          <br></br>
          <label>
              Name:
            <input type="text" required ref={this.textInput} />
          </label>
          <input type="submit" value="Submit" />
        </form>
        {isSubmitted
            ? <Flavor name = {this.state.name} value = {this.state.value} />
            : null
        }
        </div>
      );
    }
  }
export default FlavorForm;