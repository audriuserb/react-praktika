import React from 'react';
class Reservation extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        fields: [],
      };
      this.handleInputChange = this.handleInputChange.bind(this);
    }
  
    handleInputChange(event) {
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked.toString() : target.value;
      const name = target.name;
      const fields = this.state.fields;
      fields[name] = value;
      this.setState({
        fields
      });
    }
  
    render() {
      return (
        <div>
        <form onChange={this.handleInputChange}>
          <label>
            Is going:
            <input
              name="isGoing"
              type="checkbox"
              checked={this.state.isGoing}/>
          </label>
          <br />
          <label>
            Number of guests:
            <input
              name="numberOfGuests"
              type="number"
              value={this.state.numberOfGuests} />
          </label>
        </form>
        <div>
          <ul>
          {
            Object.keys(this.state.fields).map((name) => <li key={name}>{name+': '}{this.state.fields[name]}</li>)
          }
          </ul>
        </div>
        </div>
      );
    }
  }
export default Reservation;