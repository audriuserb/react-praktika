import React from 'react';
class Toggle extends React.Component {
    constructor(props) {
      super(props);
      this.state = {isToggleOn: true, date: new Date()};
  
      // This binding is necessary to make `this` work in the callback
      this.handleClick = this.handleClick.bind(this);
    }
  
    handleClick() {
      this.setState(prevState => ({
        isToggleOn: !prevState.isToggleOn
      }));
    }
  
    render() {
        const date = this.state.date.toLocaleDateString();
        const time = this.state.date.toLocaleTimeString();
      return (
          <div>
        <button onClick={this.handleClick}>
          {this.state.isToggleOn ? 'Laikas' : 'Data'}
        </button>
        <div>
            {
            this.state.isToggleOn? date : time
            }
        </div>
        </div>
      );
    }
  }
  
export default Toggle;