import React from 'react';
import UserGreeting from './usergreeting';
import GuestGreeting from './guestgreeting';
import UserLogout from './logoutmessage';
function Greeting(props) {
    const isLoggedIn = props.isLoggedIn;
    const logoutMessage = props.logoutMessage;
    if (isLoggedIn && logoutMessage) {
      return <UserGreeting />;
    }
    else if(!logoutMessage && isLoggedIn){
        return <UserLogout />;
    }
    else 
    return <GuestGreeting />;
  }
export default Greeting;