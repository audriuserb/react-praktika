import React from 'react';
import Greeting from './greeting';
import LoginButton from './loginbutton';
import LogoutButton from './logoutbutton';
class LoginControl extends React.Component {
    constructor(props) {
      super(props);
      this.handleLoginClick = this.handleLoginClick.bind(this);
      this.handleLogoutClick = this.handleLogoutClick.bind(this);
      this.handleLogoutMessage = this.handleLogoutMessage.bind(this);
      this.state = {isLoggedIn: false, logoutMessage: false};
    }
  
    handleLoginClick() {
      this.setState({isLoggedIn: true, logoutMessage: true});
    }
  
    handleLogoutClick() {
      this.setState({isLoggedIn: false, logoutMessage:false});
    }

    handleLogoutMessage(){
        this.setState({isLoggedIn: true, logoutMessage:false});
    }
    
    render() {
      const isLoggedIn = this.state.isLoggedIn;
      const logoutMessage = this.state.logoutMessage;
      let button;
  
      if (isLoggedIn) {
        if(logoutMessage){
            button = <LogoutButton onClick={this.handleLogoutMessage} />;
        }
        else{
            button = <LogoutButton onClick={this.handleLogoutClick} />;
        }
      } 
      else {
        button = <LoginButton onClick={this.handleLoginClick} />;
      }
  
      return (
        <div>
          <Greeting isLoggedIn={isLoggedIn} logoutMessage={logoutMessage} />,
          {button}
        </div>
      );
    }
  }
export default LoginControl;