import React from 'react';
import ReactDOM from 'react-dom';
import Mailbox from './mailbox';
import MessageList from './messagelist';
ReactDOM.render(
  <Mailbox />,
  document.getElementById('app')
);