import React from 'react';
import ReactDOM from 'react-dom';
import MessageList from './messagelist';
class Mailbox extends React.Component{
    constructor(props) {
        super(props);
        this.state = {messageClicked: false}
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState({messageClicked: true})
    }

    render() {
    const messageClicked = this.state.messageClicked;
    const unreadMessages = ['React', 'Re: React', 'Re:Re: React'];
    return(
      <div>
        <h1>Hello!</h1>
        {unreadMessages.length > 0 &&
          <h2 onClick={this.handleClick}>
            You have {unreadMessages.length} unread messages.
          </h2>
        }
        <MessageList unreadMessages={unreadMessages} messageClicked={messageClicked}/>
      </div>);
    }
  }
export default Mailbox;