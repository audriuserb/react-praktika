import React from 'react';
function MessageList(props){
    const unreadMessages = props.unreadMessages;
    const messageClicked = props.messageClicked;
    if(messageClicked){
        return (
        <ol>
            {unreadMessages.map(x => <li>{x}</li>)}
        </ol>
        );}
    else return(null);
}
export default MessageList;