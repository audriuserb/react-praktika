import React from 'react';
class Page extends React.Component {
    constructor(props) {
      super(props);
      this.state = {showWarning: true, showXWarning: true};
      this.handleToggleClick = this.handleToggleClick.bind(this);
      this.handleXToggleClick = this.handleXToggleClick.bind(this);
    }
  
    handleToggleClick() {
      this.setState(prevState => ({
        showWarning: !prevState.showWarning
      }));
    }

    handleXToggleClick() {
        this.setState(prevState => ({
          showXWarning: !prevState.showXWarning
        }));
      }
    
    render() {
        const showWarning = this.state.showWarning;
        const showXWarning = this.state.showXWarning;
            return(
            <div>
            {showWarning
            ? <div className="warning">
                <button id='xbutton' onClick={this.handleXToggleClick}>
                    x
                </button>
            </div>
            : null
            }
            {
            showXWarning
            ? <button id='showbutton' onClick={this.handleToggleClick}>
                {this.state.showWarning ? 'Hide' : 'Show'}
            </button>
            : null
            }
            </div>
            );
        }
    }

export default Page;  