import React from 'react';
function CurrentDate(props) {
  return <h2>Today is {props.date.toLocaleDateString("en-US")}.</h2>;
}

export default CurrentDate;