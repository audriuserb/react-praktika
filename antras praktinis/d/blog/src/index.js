import React from 'react';
import ReactDOM from 'react-dom';
import Blog from './blog';
const numbers = [1, 2, 3, 4, 5];
const posts = [
  {id: 1, title: 'Hello World', content: 'Welcome to learning React!'},
  {id: 2, title: 'Installation', content: 'You can install React from npm.'},
  {id: 3, title: 'Namų darbai', content: 'mokausi js react.'}
];
ReactDOM.render(
  <Blog posts={posts} />,
  document.getElementById('app')
);