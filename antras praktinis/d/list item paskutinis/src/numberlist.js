import React from 'react';
import ListItem from './listitem';
function NumberList(props) {
    const numbers = props.numbers;
    return (
      <ul>
        {numbers.map((number) =>
          <ListItem key={number.toString()}
                    value={number+10} />
        )}
      </ul>
    );
  }
export default NumberList;