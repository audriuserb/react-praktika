import React from 'react';
function NumberList(props) {
    const numbers = props.numbers;
    const listItems = numbers.map((number) =>
      <li key={number.toString()}>
        {number}
      </li>
    );
    return (
      <ol>{listItems}</ol>
    );
  }
export default NumberList;