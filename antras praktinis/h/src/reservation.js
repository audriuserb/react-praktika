import React from 'react';
class Reservation extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        colorType: {},
        color:[],
        numberOfGuests: ''
      };
      this.colorForm = React.createRef();
      this.textRef = React.createRef();
      this.inputRef = React.createRef();
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleColorSubmit = this.handleColorSubmit.bind(this);
      this.colorChange = this.colorChange.bind(this);
    }

    handleSubmit(event) {
      this.setState({isSubmitted: true, numberOfGuests: this.textRef.current.value});
      event.preventDefault();
  }

    colorChange(event){
      this.setState({colorType: event.target.current});
      event.preventDefault();
    }

  handleColorSubmit(event) {
    event.preventDefault();
    const {color} = this.colorForm
    //console.log(this.inputRef)
   // const colorArray = Array.prototype.slice.call(this.state.colorType);
  //  console.log(colorArray)
    for (let [key, element] of Object.entries(this.colorForm.current)) {
    if(element.type === 'text'){
      this.state.color.push(element.value);
    console.log(element.value);
    }
    }
    this.setState({});
}

    multipleInputs(){
      const inputs = [];
      for(var i=0; i<this.state.numberOfGuests; i++){
      inputs.push(<div><label>Įveskite {i+1} spalvą:<input type="text"/></label><br></br></div>)}
      return(
        <div>
          {inputs}
        </div>
      )} 

    render() {
      const isSubmitted = this.state.isSubmitted;
      const isColorSubmitted = this.state.isColorSubmitted;
      return (
        <div>
        <form onSubmit={this.handleSubmit}>
          <br />
          <label>
            Number of guests:
            <input
              name="numberOfGuests"
              type="number"
              ref={this.textRef}/>
          </label>
            <input type="submit" value="Submit" />
        </form>
        {isSubmitted
        ?<div>
        <form onSubmit={this.handleColorSubmit} ref={this.colorForm}>
            {this.multipleInputs()}
            <input type="submit" value="Sukelti spalvas" />
          </form>
        <div>
            {
              this.state.color.map(x => x+', ')
            }
        </div>
        </div>
        : null
        }
        </div>
      );
    }
}
export default Reservation;