import React, {Component} from 'react';

class App extends Component{
    constructor(props){
        super(props)
        this.onHandleUsers = this.onHandleUsers.bind(this)
        this.state = {
            isLoaded: false,
            users: []
        }
        this.selectedUser = React.createRef()
    }

    onHandleCurrentUser(){
        const userId = this.selectedUser.current.value;
        event.preventDefault();
        const data = {userId: userId}
        fetch("http://localhost:3000/api/user", {
            method: 'POST',
            headers: {"Content-Type" : "application/json"},
            body: JSON.stringify(data)
        })
        .then(res => res.json())
        .then( result => {
            this.setState({
                isLoaded: false,
                currentUser: result
            })
        })
        .catch(e => console.log(e))
    }

    onHandleUsers() {
        fetch('http://localhost:3000/api/users')
        .then(res => res.json())
        .then(result => {
            this.setState({
                isLoaded: true,
                users: result
            })
        })
        .catch(e => console.log(e))
    }

    render(){
        const {currentUser} = this.state
        return (
            <div>
                <h1>Sveikas, Panevėžy!</h1>
                <button onClick={this.onHandleUsers}>Gauti vartotojus</button>
                <ul>
                    {
                        this.state.isLoaded && this.state.users.map(item =>{
                            return (
                                <li>
                                    Vardas: {item.name},<br></br>
                                    pavardė: {item.surname}<br></br>
                                </li>
                            )
                        })
                    }
                </ul>
                <input type='number' ref={this.selectedUser}></input>
                <button onClick={this.onHandleCurrentUser}>Gauti antrą</button>
                <div>
                    {
                        currentUser && <div>{currentUser.name} {currentUser.surname}</div>
                    }
                </div>
            </div>
        )
    }
}

export default App;