class UserApiController{
    constructor(){
        this.getUsers = this.getUsers.bind(this)
    }
    
    __usersList(){
        return [
            {id: 1, name: 'Jonas', surname: 'Grybas'},
            {id: 2, name: 'Ernestas', surname: 'Arlauskas'}
        ]
    }
    
    getUsers(req, res, next){
        const users = this.__usersList()
        return res.json(users)
    }
}

module.exports = UserApiController
